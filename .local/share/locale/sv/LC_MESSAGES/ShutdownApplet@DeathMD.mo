��          \      �       �   7   �           	               &     6  �  >  Y   �     7  	   @     J  	   X     b     w                                       An applet to shutdown, restart and suspend your system. Log Out Restart Screen Lock Shutdown Shutdown Applet Suspend Project-Id-Version: ShutdownApplet@DeathMD
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-05-23 17:29+0200
Language-Team: Svenska Språkfiler <contactform@svenskasprakfiler.se>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
Last-Translator: Åke Engelbrektson <eson@svenskasprakfiler.se>
Plural-Forms: nplurals=2; plural=(n != 1);
Language: sv
 Ett panelprogram för att stänga av, starta om och försätta ditt system i vänteläge. Logga ut Starta om Lås skärmen Stäng av Avstängningsprogram Vänteläge 