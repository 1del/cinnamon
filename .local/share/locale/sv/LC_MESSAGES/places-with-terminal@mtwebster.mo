��    
      l      �       �   G   �   !   9     [     v          �     �     �     �  �  �  I   u  (   �     �     �  	             &     E     ]               
      	                        Access your places and bookmarks, or launch a terminal at that location Add terminal button to menu items Change default programs... Computer File System Places and bookmarks Places with Terminal Launcher Refresh bookmarks... icon Project-Id-Version: places-with-terminal
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-05-13 08:39+0200
Language-Team: Svenska Språkfiler <contactform@svenskasprakfiler.se>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
Last-Translator: Åke Engelbrektson <eson@svenskasprakfiler.se>
Plural-Forms: nplurals=2; plural=(n != 1);
Language: sv
 Öppna dina platser och bokmärken, eller starta en terminal i den mappen Lägg till en terminalknapp i menyposter Byt standardprogram... Dator Filsystem Platser och bokmärken Platser med termininalstartare Uppdatera bokmärken... Ikon 