��    ,      |  ;   �      �  "   �  
   �     �     �  .     1   ;     m     |  +   �     �  5   �          #  
   B     M     ]  	   b     l     {  �   �  	     #   %  @   I  4   �  
   �      �     �     �     �          %     E     R     W     d     m     {  j   �  �   �     �     �     �     �  T  �  )   �	     
     %
     -
  2   C
  8   v
     �
     �
  3   �
  "     :   1  $   l  '   �     �      �     �     �            �   ,     �  :   �  Q     ;   a     �     �     �     �     �     �  ,   �     )     ;     @     Q     Z     q  q   �  �        �     �     �     �                  !                    (      &      $   #      )                 	                       ,   '                                *       
                 +            %              "            A simple radio applet for Cinnamon Appearance Bicolor Bicolor Icon Can't copy current Song. Is the Radio playing? Can't increase Volume. Volume already at maximum. Can't play  %s Cancel download Color of Symbolic Icon when playing a radio Copy current song title Do you want to proceed the download at your own risk? Download Confirmation Dialog Download plugin at my own risk Full Color Full Color Icon Icon Icon Type Initial Volume List of stations Make sure that the URL is valid and you have a stable internet connection. Don't hestitate to open an Issue on Github if the problem persists. More info Open radio stream URL search engine Opens a website, which helps you to determine radio stream URLs. Opens a website, which lists URLs of radio stations. Playing %s Please install the '%s' package. Preferences Radio Radio++ Show Radio Channel on the panel Show URL-list of radio stations Show in list Stop Stop Radio++ Symbolic Symbolic Icon Symbolic Icon Color The initial volume is applied when clicking on a radio stream and no other radio stream is already running The radio applet depends on the '%s' plugin. It's a 3rd party plugin for mpv, which allows controlling the radio player with the sound applet. Title Type URL Volume Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-01-16 15:10+0100
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
Last-Translator: 
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n==0 || (n!=1 && n%100>=1 && n%100<=19) ? 1 : 2);
Language: ro
 Un applet simplu de radio pentru Cinnamon Aspect Bicolor Pictogramă bicoloră Nu se poate copia cântecul curent. Merge radioul? Nu se poate crește volumul. Volumul este deja la maxim. Nu se poate reda %s Anulează descărcarea Culoarea pictogramei simbolice când se redă radio Copiază titlul cântecului curent Dorești să continui descărcarea pe propria răspundere? Dialog de confirmare a descărcării Descarcă plugin pe propria răspundere Culoare completă Pictogramă cu culoare completă Pictogramă Tip de pictogramă Volum inițial Listă de posturi Asigură-te că URL-ul este valid și că ai o conexiune stabilă la internet. Nu ezita să deschizi un Issue pe GitHub dacă problema persistă. Mai multe informații Deschide motor de căutare pentru URL-uri de fluxuri radio Deschide un site, care te poate ajuta să găsești URL-uri pentru fluxuri radio. Deschide un site care enumeră URL-uri pentru posturi radio Se redă %s Instalează pachetul '%s'. Preferințe Radio Radio++ Arată Canalul Radio pe panou Arată lista de URL-uri pentru posturi radio Arată în listă Stop Oprește Radio++ Simbolic Pictogramă simbolică Culoare pictogramă simbolică Volumul inițial se aplică atunci când se dă click pe un flux radio și niciun alt flux radio nu rulează deja Appletul radio depinde de pluginul '%s'. Este un plugin extern pentru mpv, care permite controlarea playerului radio cu appletul de sunet. Titlu Tip URL Volum 