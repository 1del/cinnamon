��    T      �  q   \            !     %     2     ;     A     O     V  $   [     �     �     �     �     �     �     �     �     �     �  
   �     �  	   �  	   �     �               /     ;     W     r     �     �     �     �     �     �     �     �     �     �     	  	   	     	     !	     $	     '	     0	     B	     Y	     q	     z	     �	  @   �	     �	     �	     �	      
     
     
      
     '
     -
     5
     <
     K
     X
     g
     u
     ~
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
     �
  
   �
     �
             �       �     �                    (     1  (   6  	   _     i     n     p     |     �     �     �  	   �     �     �     �  
   �     �     �                -     :     Z     s     �  
   �     �     �     �     �     �     �     �     �     �  	   �          
       
             ,     >     P     X  ,   g  <   �     �  
   �  
   �     �     �     	  	        &  	   /     9     G     ]     k     �     �     �     �     �     �     �     �     �     �  )   �               #  
   +     6     >  	   B     L   /      .                  9   <      H      T              5          '   M              N              -          0               :          @   ,   O   ;   $   F          S   "   6          G       3   	       %         *             )   R              1   #              A      D      J   E   ?   !   +   Q   I      (      K       >   &       C   
             8          =      2   B   4          7          P    ... Blowing snow Blustery Clear Click to open Cloudy Cold Display current temperature in panel Drizzle Dust E Fair Foggy Freezing drizzle Freezing rain Friday Hail Haze Heavy snow Hot Humidity: Hurricane Isolated thundershowers Isolated thunderstorms Light snow showers Loading ... Loading current weather ... Loading future weather ... Mixed rain and hail Mixed rain and sleet Mixed rain and snow Mixed snow and sleet Monday Mostly cloudy N NE NW Not available Override location label Partly cloudy Pressure: S SE SW Saturday Scattered showers Scattered snow showers Scattered thunderstorms Settings Severe thunderstorms Show sunrise / sunset times Show the weather condition (e.g., "Windy", "Clear") in the panel Showers Sleet Smoky Snow Snow flurries Snow showers Sunday Sunny Sunrise Sunset Symbolic icons Temperature: Thundershowers Thunderstorms Thursday Tornado Tropical storm Tuesday W WOEID Wednesday Where On Earth ID Wind Chill: Wind speed unit Wind: Windy celsius fahrenheit kph m/s mph Project-Id-Version: simon04-gnome-shell-extension-weather
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-02-16 11:25+0100
Last-Translator: Marian Vasile <marianvasile@upcmail.ro> și Dorian Baciu
Language-Team: 
Language: ro_RO
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n == 1 ? 0: (((n % 100 > 19) || ((n % 100 == 0) && (n != 0))) ? 2: 1));\n";
X-Poedit-SourceCharset: utf-8
X-Generator: Poedit 2.3
 ... Ninsoare viscolită Zgomotos Senin Clic pentru a deschide Înnorat Rece Afișarea temperaturii curente în panou Burniță Praf E Vreme bună Ceață Burniță rece Ploaie rece Vineri Grindină Ceață ușoară Ninsoare abundentă Foarte cald Umiditate: Uragan Ploi izolate Furtuni izolate Ninsoare ușoară în averse Se încarcă Se încarcă vremea curentă... Se încarcă prognoza... Ploaie cu grindină Măzăriche Lapoviță Măzăriche Luni Majoritar înnorat N NE NV Indisponibil Suprapunere locație Parțial înnorat Presiune: S SE SV Sâmbătă Averse răzlețe Ninsori răzlețe Furtuni răzlețe Setări Furtuni severe Arată timpul pentru răsărit/apus de soare Arată condițiile vremii (de ex. "Vânt", "Clar") în panou Averse Lapoviță Întunecat Ninsoare Ninsoare ușoară Ninsori în averse Duminică Însorit Răsărit Apus de soare Pictograme (iconițe) Temperatură: Ploi abundente în averse Furtuni Joi Tornadă Furtună tropicală Marți V WOEID Miercuri Locația pe Pământ Vânt: Initatea de măsură pt. viteza vântului Vânt: Rafale de vânt celsius fahrenheit km/oră m/s mile/oră 