��    i      d  �   �       	     	     	     	     ,	     5	     ;	     I	     P	  $   U	     z	     �	     �	     �	     �	     �	     �	     �	     �	     �	  	   �	     �	     �	  
   �	     
  	   
  	   
      
     8
     O
     b
     n
     �
     �
     �
     �
     �
     �
     �
                         "     :     =  	   K     U     ]     _     b     e     n     �     �     �     �     �  @   �     *     2     8     >     C     Q     ^     e     k     s     z     �     �     �     �     �     �     �     �     �                 %     F     H     N  	   V     `     l     |     �     �     �     �     �  
   �     �     �     �     �     �     �     �     �     �     �  b  �     =     A     Q  	   X     b     i     p     w  !   ~     �     �     �     �     �     �     �     �     �  	             %     )     0     7     >     F     V     f     �     �  .   �  +   �  	          	        (  	   8     B     U     Y     `     g     w     �     �     �     �     �     �     �  	   �     �     �  !        7     >     W  >   v     �  	   �  	   �     �     �     �  	   �     �  	     	     *        D     T     \     c  	   s     }     �     �  	   �     �     �  *   �     �          	  	             (     8     @     G     J     Q     X     \     c     i  	   m     w     {          �     �     �     �     e                   A   /   ;       0      9   
   F               1       6   \   R   T       #   i   B       [   L      ]                 g              Q      c       =      >      ,   J       -       Z       2   `       I      G   ^   "   <      K              @   X      a   3       M   f   (   O   +   ?   !   '   W   C      S       b   .   &   P                  D   8           :   H   %   4   N         Y                                          	   d       _      )   $      E       h          5   V           U                     *       7              ... Atmospheric pressure unit Blowing snow Blustery Clear Click to open Cloudy Cold Display current temperature in panel Display time in 24 hour format Drizzle Dust E Fair Foggy Forecast length Freezing drizzle Freezing rain Friday Get WOEID Hail Haze Heavy snow Hot Humidity: Hurricane Isolated thundershowers Isolated thunderstorms Light snow showers Loading ... Loading current weather ... Loading future weather ... Mixed rain and hail Mixed rain and sleet Mixed rain and snow Mixed snow and sleet Monday Mostly cloudy N NE NW Not available Override location label Pa Partly cloudy Pressure: Refresh S SE SW Saturday Scattered showers Scattered snow showers Scattered thunderstorms Settings Severe thunderstorms Show sunrise / sunset times Show the weather condition (e.g., "Windy", "Clear") in the panel Showers Sleet Smoky Snow Snow flurries Snow showers Sunday Sunny Sunrise Sunset Symbolic icons Temperature unit Temperature: Thundershowers Thunderstorms Thursday Tornado Translate condition Tropical storm Tuesday Update interval Vertical orientation View your local weather forecast W WOEID Weather Wednesday Wind Chill: Wind speed unit Wind: Windy at atm celsius days fahrenheit in Hg kPa knots kph m/s mbar minutes mm Hg mph psi Project-Id-Version: gnome-shell-extension-weather 1.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-02-16 11:25+0100
Last-Translator: Takeshi AIHANA <takeshi.aihana@gmail.com>
Language-Team: Japanese <takeshi.aihana@gmail.com>
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
 ... 気圧の単位 吹雪 大荒れ 快晴 開く 曇り 寒冷 パネルに気温を表示する 24時間表示 霧雨 砂塵 東 晴れ 霧 天気予報の表示日数 着氷性の霧雨 着氷性の雨 金曜日 WOEID を取得 雹 もや 大雪 温暖 湿度: ハリケーン 局地的雷雨 局地的な激しい雷雨 弱いにわか雪 読み込み中 ... 現在の天気を読み込んでいます ... 天気予報を読み込んでいます ... 雨と雹 雨とみぞれ 雨と雪 雪とみぞれ 月曜日 おおむね曇り 北 北東 北西 データなし 場所ラベルの書換 Pa ところにより曇り 気圧: 更新 南 南東 南西 土曜日 ところによりにわか雨 ところによりにわか雪 ところにより激しい雷雨 設定 非常に激しい雷雨 日の出と日の入の時刻 パネルに天気の状態(例："強風", "晴れ")を表示 にわか雨 みぞれ けむり 雪 にわか雪 にわか雪 日曜日 晴れ 日の出 日の入 シンボリック・アイコンにする 気温の単位 気温: 雷雨 激しい雷雨 木曜日 竜巻 状態を日本語で表示 熱帯暴風雨 火曜日 更新間隔 縦に配置 指定した場所の天気予報を表示 西 WOEID 天気 水曜日 体感温度: 風速の単位 風速: 強風 at 気圧 摂氏 日 華氏 in Hg kPa ノット k/h m/s hPa 分 mm Hg m/h psi 