��    X      �     �      �     �     �     �     �     �     �     �  $   �     �                               #     1  	   8     B     G  
   L     W  	   [  	   e     o     �     �     �     �     �     �     	     	     1	     F	     M	     [	     ]	     `	     c	     q	     �	  	   �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
  @   -
     n
     v
     |
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
                    -     <     D     F  	   L     V     h     t     �     �     �  
   �     �     �     �     �  |  �     2     6  
   J     U     \  	   o     y  $     	   �     �     �     �  	   �     �     �     �     �  
   �       	                  *     2     P     h     ~     �  #   �     �     �     �       
        (     ;     =     @     C     W     v     �     �     �     �  	   �     �     �     �  	   �     �  $     @   5  
   v  
   �     �     �  
   �     �     �  
   �  
   �  
   �     �     �               '     7     @     I     W  	   h     r     t  
   z     �     �     �     �  	   �     �  
   �     �     �     �          <                 T   !          D   ;       F   6       G          	   ?   J   E   -      C   S          Q          A                                      "       7   B   &      L                  $   1       N      .       R   *           '                       3   
           I   8   %          >   X      O   M   @   K   +           W                :   #   5          =       H       P      V           9   /      4       0           U                  )   ,   (   2    ... Blowing snow Blustery Clear Click to open Cloudy Cold Display current temperature in panel Drizzle Dust E Fair Foggy Freezing drizzle Freezing rain Friday Get WOEID Hail Haze Heavy snow Hot Humidity: Hurricane Isolated thundershowers Isolated thunderstorms Light snow showers Loading ... Loading current weather ... Loading future weather ... Mixed rain and hail Mixed rain and sleet Mixed rain and snow Mixed snow and sleet Monday Mostly cloudy N NE NW Not available Override location label Partly cloudy Pressure: S SE SW Saturday Scattered showers Scattered snow showers Scattered thunderstorms Settings Severe thunderstorms Show sunrise / sunset times Show the weather condition (e.g., "Windy", "Clear") in the panel Showers Sleet Smoky Snow Snow flurries Snow showers Sunday Sunny Sunrise Sunset Symbolic icons Temperature unit Temperature: Thundershowers Thunderstorms Thursday Tornado Translate condition Tropical storm Tuesday W WOEID Wednesday Where On Earth ID Wind Chill: Wind speed unit Wind: Windy celsius fahrenheit knots kph m/s mph Project-Id-Version: 0.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2014-09-20 23:11+0200
Last-Translator: Asier Iturralde Sarasola <asier.iturralde@gmail.com>
Language-Team: Librezale <librezale@librezale.org
Language: eu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Virtaal 0.7.1
 ... Elur-jasa haizetsua Ekaitztsua Garbia Klikatu irekitzeko Hodeitsua Hotza Bistaratu uneko tenperatura panelean Zirimiria Hautsa E Ona Lainotsua Zirimiri izoztua Euri izoztua Ostirala Eskuratu WOEIDa Kazkabarra Gandua Elur-jasa Beroa Hezetasuna: Urakana Zaparrada ekaitztsu isolatuak Trumoi-ekaitz isolatuak Elur-zaparrada arinak Kargatzen... Uneko eguraldia kargatzen... Etorkizuneko eguraldia kargatzen... Euria eta kazkabarra Euria eta elurbustia Euria eta elurra Elurra eta elurbustia Astelehena Nagusiki hodeitsua I IE IM Ez dago erabilgarri Gainidatzi kokapenaren etiketa Nahiko hodeitsua Presioa: H HE HM Larunbata Zaparrada sakabanatuak Elur-zaparrada sakabanatuak Trumoi-ekaitz sakabanatuak Ezarpenak Trumoi-ekaitz gogorra Erakutsi egunsenti / ilunabar orduak Erakutsi eguraldiaren egoera (adb. "Haizetsu", "Garbi") panelean Zaparradak Elurbustia Ketsua Elurra Elur-jasak Elur-zaparradak Igandea Eguzkitsua Egunsentia Ilunabarra Ikono sinbolikoak Tenperatura unitatea Tenperatura: Zaparrada ekaiztsuak Trumoi-ekaitzak Osteguna Tornadoa Itzuli egoera Ekaitz tropikala Asteartea M WOEID Asteazkena Where On Earth ID Haizearen sentsazioa: Haizearen abiaduraren unitatea Haizea: Haizetsua celsius fahrenheit korapilo km/h m/s mph 