��          �      �       H  '   I     q  %   �     �     �  *   �     �  	   �  �   �          �     �     �  B  �  C         D  %   c     �     �  -   �     �     �  �   �  )   �     �     �     �     	                            
                                Click on the applet to turn off monitor Color of the symbolic icon Duration of deactivation of the mouse General Icon Keyboard shortcut to turn off your monitor Mouse Shortcuts To prevent the monitor from turning on too soon after turning it off, the mouse will be disabled for the number of seconds set here. Turn Off Monitor Turn off monitor Use a symbolic icon seconds Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-12-01 20:08+0100
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
Last-Translator: Claudiux <claude.clerc@gmail.com>
Plural-Forms: nplurals=2; plural=(n > 1);
Language: fr
 Éteint l'écran d'un clic sur l'icône ou par un raccourci-clavier Couleur de l'icône symbolique Durée de désactivation de la souris Paramètres généraux Icône Raccourci-clavier pour éteindre votre écran Souris Raccourcis-clavier Pour éviter que l'écran ne se rallume trop tôt après l'avoir éteint, la souris sera désactivée pendant le nombre de secondes défini ici. Extinction de l'écran (Turn Off Monitor) Éteindre l'écran Utiliser une icône symbolique secondes 