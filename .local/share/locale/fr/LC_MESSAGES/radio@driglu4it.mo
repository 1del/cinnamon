��          �      <      �     �  +   �     �     �     �       
         +     1     9     >     K     Y  e   m     �     �     �  B  �     %  ;   5     q     �     �     �     �     �     �     �     �     �        �        �     �     �                  
                                                	                                   Bicolor Icon Color of Symbolic Icon when playing a radio Full Color Icon Icon Include of list List of stations Playing %s Radio Radio++ Stop Stop Radio++ Symbolic Icon Symbolic Icon Color This simple radio for Cinnamon. Require moc player. In setting you may add, edit and remove stations. Title Type URL Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-14 23:49+0100
Last-Translator: Claudiux <claude.clerc@gmail.com>
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
Plural-Forms: nplurals=2; plural=(n > 1);
 Icône bicolore Couleur de l'icône symbolique durant l'écoute d'une radio Icône en couleurs Icône Afficher dans le menu Liste des stations À l'écoute de %s Radio Radio++ Arrêt Arrêt de Radio++ Icône symbolique Couleur de l'icône symbolique Simple Poste de Radio pour Cinnamon. Nécessite l'installation du lecteur audio moc. Dans la configuration vous pouvez ajouter, éditer et supprimer des stations de web-radios. Titre Type URL 