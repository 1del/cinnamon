��          �      �       H  '   I     q  %   �     �     �  *   �     �  	   �  �   �          �     �     �  M  �           ,  !   J     l     u  (   z     �     �  �   �     5     C     Q     d     	                            
                                Click on the applet to turn off monitor Color of the symbolic icon Duration of deactivation of the mouse General Icon Keyboard shortcut to turn off your monitor Mouse Shortcuts To prevent the monitor from turning on too soon after turning it off, the mouse will be disabled for the number of seconds set here. Turn Off Monitor Turn off monitor Use a symbolic icon seconds Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-12-07 16:49+0100
Last-Translator: Alan Mortensen <alanmortensen.am@gmail.com>
Language-Team: 
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
Plural-Forms: nplurals=2; plural=(n != 1);
 Sluk skærmen med et enkelt klik Farven på det symbolske ikon Varighed af deaktivering af musen Generelt Ikon Tastaturgenvej til at at slukke skærmen Mus Genveje For at undgå at skærmen tændes for hurtigt, efter den er slukket, vil musen være deaktiveret i det antal sekunder indstillet her. Sluk skærmen Sluk skærmen Brug symbolsk ikon sekunder 