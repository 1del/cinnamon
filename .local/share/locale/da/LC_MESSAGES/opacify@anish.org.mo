��    	      d      �       �      �      �   B   �      7     G     O     ]     m  K  z     �     �  O   �     5     P     b     w     �                          	                    Effect End of Drag Fade out the current window that the user is resizing or dragging. Opacify Windows Opacity Start of Drag Transition time milliseconds Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-08-16 19:28+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
Last-Translator: Alan Mortensen <alanmortensen.am@gmail.com>
Plural-Forms: nplurals=2; plural=(n != 1);
Language: da
 Effekt Slutningen på trækket Gør vinduet, som flyttes eller hvis størrelse ændres, delvist gennemsigtigt. Gør vinduer gennemsigtige Uigennemsigtighed Starten på trækket Overgangstid millisekunder 