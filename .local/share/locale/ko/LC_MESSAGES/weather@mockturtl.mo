��    ]           �      �     �     �                    #     1     8  $   =     b     �     �     �     �     �     �     �     �     �  	   �     �     �  
   �     �  	   �  	   �     	      	     7	     J	     V	     r	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     

     "
  	   0
     :
     B
     D
     G
     J
     S
     e
     |
     �
     �
     �
  @   �
                    #     (     6     C     J     P     X     _     n          �     �     �     �     �     �     �     �     �      
     +     -  	   5     ?     Q     ]     m     s     y     �  
   �     �  =  �     �     �  
   �     �          	                    =  	   I     S     Z     a     h     o     �     �  	   �     �     �     �     �     �     �     �     �               )  .   >  .   m     �     �  
   �     �  	   �     �     �  	   �  	             %     <     J     R     _  	   f  	   p  	   z     �     �     �     �     �     �  9   �  	   *     4     A     H  	   L  	   V  	   `     j     q     x          �     �     �     �  	   �     �     �     �  	   �            0   "     S     Z  	   a      k     �     �     �     �     �     �     �     �     @       	          \             H   ?       J   :   P   K          
   C   N   I   1   !   G   Z          X   /      E                                     %       ;   F   )                          '   5   ]   U      2   $   Y   -       S   *                       7         [   M   <   (          B   W      V   T   D   R   .                  "   O      >   &   9          A       L       Q                  =   3      8       4       #                      ,   0   +   6    ... Atmospheric pressure unit Blowing snow Blustery Clear Click to open Cloudy Cold Display current temperature in panel Display time in 24 hour format Drizzle Dust E Fair Foggy Forecast length Freezing drizzle Freezing rain Friday Get WOEID Hail Haze Heavy snow Hot Humidity: Hurricane Isolated thundershowers Isolated thunderstorms Light snow showers Loading ... Loading current weather ... Loading future weather ... Mixed rain and hail Mixed rain and sleet Mixed rain and snow Mixed snow and sleet Monday Mostly cloudy N NE NW Not available Override location label Partly cloudy Pressure: Refresh S SE SW Saturday Scattered showers Scattered snow showers Scattered thunderstorms Settings Severe thunderstorms Show sunrise / sunset times Show the weather condition (e.g., "Windy", "Clear") in the panel Showers Sleet Smoky Snow Snow flurries Snow showers Sunday Sunny Sunrise Sunset Symbolic icons Temperature unit Temperature: Thundershowers Thunderstorms Thursday Tornado Translate condition Tropical storm Tuesday Update interval Vertical orientation View your local weather forecast W Weather Wednesday Where On Earth ID Wind Chill: Wind speed unit Wind: Windy celsius days fahrenheit minutes Project-Id-Version: weather@mockturtl 1.13.7
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-02-12 14:49+0900
Last-Translator: Hang Park <hangpark@kaist.ac.kr>
Language-Team: Korean <hangpark@kaist.ac.kr>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
 ... 기압 단위 날린 눈 바람 거셈 맑음 열기 구름 추움 패널에 현재 기온 출력 24시간제 이슬비 먼지 동풍 맑음 안개 기상예보 기간 착빙성 이슬비 착빙성 비 금요일 WOEID 얻기 우박 아지랑이 폭설 더움 습도: 허리케인 고립형 소낙눈 고립형 뇌우 약한 소낙눈 가져오는 중 ... 현재 날씨를 가져오고 있습니다 ... 날씨 예보를 가져오고 있습니다 ... 비와 우박 비와 진눈깨비 비와 눈 눈과 진눈깨비 월요일 구름 많음 북풍 북동풍 북서풍 이용할 수 없음 지역명 덮어쓰기 구름 약간 기압: 새로고침 남풍 남동풍 남서풍 토요일 산재형 소나기 산재형 소낙눈 산재형 뇌우 설정 강한 뇌우 일출 / 일몰 시간 출력 패널에 날씨 상태("바람", "맑음" 등)를 출력 소나기 진눈깨비 연기 눈 소낙눈 소낙눈 일요일 화창 일출 일몰 심볼릭 아이콘 기온 단위 기온: 천둥을 동반한 비 뇌우 목요일 토네이도 번역 열대 폭풍우 화요일 업데이트 간격 세로쓰기 당신의 지역 날씨정보를 보여줍니다 서풍 날씨 수요일 위치 코드(Where On Earth ID) 체감온도: 풍속 단위 풍속: 바람 많음 섭씨 일 화씨 분 