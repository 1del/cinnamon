��    	      d      �       �      �      �   B   �      7     G     O     ]     m  -  z     �     �  K   �       	   ,     6     U     j                          	                    Effect End of Drag Fade out the current window that the user is resizing or dragging. Opacify Windows Opacity Start of Drag Transition time milliseconds Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-09-24 13:08-0300
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.0
Last-Translator: Marcelo Aof
Plural-Forms: nplurals=2; plural=(n > 1);
Language: pt_BR
 Efeito Fim do movimento da janela Enxergue a janela atual com transparência ao movê-la ou redimensioná-la. Janelas transparentes Opacidade Início do movimento da janela Tempo de transição milissegundos 