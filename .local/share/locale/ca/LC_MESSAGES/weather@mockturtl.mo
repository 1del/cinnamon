��    ;      �  O   �           	               #     )     0     5     =     B     G     M     ^     l     s     x  
   }     �  	   �  	   �     �     �     �     �     �     
     %     9     N     b     w     ~     �     �  	   �     �     �     �     �     �                    %     *     8     E     L     R     _     n     |     �     �     �  	   �     �     �     �  k  �     2	     6	     L	     X	  	   ]	     g	     l	     s	  	   x	     �	     �	     �	  	   �	  	   �	     �	     �	     �	     �	     �	     �	     �	     
     0
  $   E
  #   j
     �
     �
     �
     �
     �
               +  	   A     K     T     e      ~     �     �     �     �     �     �     �     �  
   �       	     	        &     -     5     G     O     X     ^     d                         *         &          3                                     8       (                 +   
       "   !       7   6   $          -   2   .   	      4   1   %   :                  /   5              #   0             9   ,            '      )         ;               ... Blowing snow Blustery Clear Cloudy Cold Drizzle Dust Fair Foggy Freezing drizzle Freezing rain Friday Hail Haze Heavy snow Hot Humidity: Hurricane Isolated thundershowers Isolated thunderstorms Light snow showers Loading ... Loading current weather ... Loading future weather ... Mixed rain and hail Mixed rain and sleet Mixed rain and snow Mixed snow and sleet Monday Mostly cloudy Not available Partly cloudy Pressure: Saturday Scattered showers Scattered snow showers Scattered thunderstorms Severe thunderstorms Showers Sleet Smoky Snow Snow flurries Snow showers Sunday Sunny Temperature: Thundershowers Thunderstorms Thursday Tornado Tropical storm Tuesday Wednesday Wind Chill: Wind: Windy Project-Id-Version: 3.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2011-06-15 00:41+0200
Last-Translator: Pau Iranzo <paugnu@gmail.com>
Language-Team: Softcatalà
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Virtaal 0.7.0-rc1
X-Project-Style: default
 ... Neu aixecada pel vent Tempestuós Clar Ennuvolat Fred Plugim Pols Bon temps Boirós Plugims gelats Pluja glaçada Divendres Calamarsa Boirina Nevada forta Calor Humitat: Huracà Tempestes aïllades Tempestes aïllades Precipitacions lleugeres de neu S'està carregant... S'està carregant el temps actual... S'està carregant el temps futur... Barreja de pluja i calamarsa Barreja de pluja i calamarsa Barreja de pluja i neu Barreja de neu i calamarsa Dilluns Majoritàriament ennuvolat No disponible Parcialment ennuvolat Pressió: Dissabte Pluges aïllades Xàfecs de neu dispersos Tempestes elèctriques aïllades Tempestes elèctriques severes Ruixats Aiguaneu Fumat Neu Ràfegues de neu Cobert Diumenge Assolellat Temperatura: Tempestes Tempestes Dijous Tornado Tempesta tropical Dimarts Dimecres Vent: Vent: Ventades 