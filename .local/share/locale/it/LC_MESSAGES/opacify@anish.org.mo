��    	      d      �       �      �      �   B   �      7     G     O     ]     m  K  z     �     �  L   �     2     D     M     f     x                          	                    Effect End of Drag Fade out the current window that the user is resizing or dragging. Opacify Windows Opacity Start of Drag Transition time milliseconds Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-06-03 11:15+0200
Last-Translator: Dragone2 <dragone2@risposteinformatiche.it>
Language-Team: 
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
Plural-Forms: nplurals=2; plural=(n != 1);
 Effetto Fine del Trascinamento Dissolve la finestra attuale che l'utente sta ridimensionando o trascinando. Opacizza Finestre Opacità Inizio del Trascinamento Tempo transizione millisecondi 