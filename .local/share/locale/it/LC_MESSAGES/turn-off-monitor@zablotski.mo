��          �      �       H  '   I     q  %   �     �     �  *   �     �  	   �  �   �          �     �     �     �  *   �     	  %   %     K     T  /   Z     �     �  �   �     #     2     A     X     	                            
                                Click on the applet to turn off monitor Color of the symbolic icon Duration of deactivation of the mouse General Icon Keyboard shortcut to turn off your monitor Mouse Shortcuts To prevent the monitor from turning on too soon after turning it off, the mouse will be disabled for the number of seconds set here. Turn Off Monitor Turn off monitor Use a symbolic icon seconds Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-06-03 21:35+0200
Last-Translator: Dragone2 <dragone2@risposteinformatiche.it>
Language-Team: 
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3
 Clicca sull'applet per spegnere lo schermo Colore dell'icona simbolica Durata della disattivazione del mouse Generale Icona Scorciatoia da tastiera per spegnere il monitor Mouse Tasti Per evitare che il monitor si accenda troppo presto dopo averlo spento, il mouse verrà disabilitato per il numero di secondi qui impostato. Spegni Schermo Spegni schermo Usa un'icona simbolica secondi 